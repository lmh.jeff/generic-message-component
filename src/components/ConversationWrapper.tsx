import React from "react";
import Text from './Text';
import Image from './Image';
import File from './File';
import { IMessage, MessageProps } from "../types/messageType";
import moment from "moment";



const ConversationWrapper = (props: MessageProps) => {
  const { conversationData } = props;

  return (
    <div className="conversationWrapper">
      {conversationData.map((conversation: IMessage) => {
        const {
          id,
          senderType,
          createdAt,
        } = conversation;

        const contentTypeComponent = () => {
          switch (conversation.contentType) {
            case "text":
              return <Text content={conversation.content} />;
            case "image":
              return <Image content={conversation.content} />;
            case "attachment":
              return <File content={conversation.content} />;
          }
        }

        return (
          <div className={`messageBox ${senderType}`} key={id}>
            {contentTypeComponent()}
            <div className="time">
                {moment(createdAt).format("MMM Do h:mm a")}
              </div>
          </div>
        )
        
      })}
    </div>
  );
};

export default ConversationWrapper;
