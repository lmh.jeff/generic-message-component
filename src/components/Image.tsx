import React from "react";
import { IURLContent } from "../types/messageType";

const Image = (props: Pick<IURLContent, "content">) => {
  const { content } = props;
  return (
    <>
      <img src={content.url}></img>
    </>
  );
};

export default Image;
