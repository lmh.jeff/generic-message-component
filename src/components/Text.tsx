import React from "react";
import { ITextContent } from "../types/messageType";

const Text = (props: Pick<ITextContent, "content">) => {
  const { content } = props;

  return <div>{content.text}</div>;
};

export default Text;
