import React from "react";
import { IFileContent } from "../types/messageType";

const File = (props: Pick<IFileContent, "content">) => {
  const { content } = props;

  return (
    <>
      <a href={content.file} download>
        <img className="attachment" src="/file-download-solid.svg" />
      </a>
    </>
  );
};

export default File;
