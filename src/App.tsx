import React from 'react';
import './App.css';
import ConversationWrapper from './components/ConversationWrapper';
/** Conversation Data */
import conversationData from './fixtures/conversation';
import { IMessage } from './types/messageType';

function App() {
  return (
    <div className="App">
      <ConversationWrapper conversationData={conversationData as IMessage[]} />
    </div>
  );
}

export default App;
