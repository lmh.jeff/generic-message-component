export interface IBasicMessage {
  id: string;
  senderType: "agent" | "user";
  createdAt: number;
}

export interface ITextContent extends IBasicMessage {
  content: Record<"text", string>;
  contentType: "text";
}

export interface IURLContent extends IBasicMessage {
  content: Record<"url", string>;
  contentType: "image";
}

export interface IFileContent extends IBasicMessage {
  content: Record<"file", string>;
  contentType: "attachment";
}

export type IMessage = ITextContent | IURLContent | IFileContent;

export interface MessageProps {
  conversationData: IMessage[];
}
